---- Minecraft Crash Report ----

WARNING: coremods are present:
  SpongeCoremod (spongeforge-1.12.2-2838-7.1.7.jar)
Contact their authors BEFORE contacting forge

// You should try our sister game, Minceraft!

Time: 17.12.19 3:13
Description: Exception in server tick loop

Missing Mods:
	unknown : need [4.0,): have missing

net.minecraftforge.fml.common.MissingModsException: Mod virtualchest (VirtualChest) requires [placeholderapi@[4.0,)]
	at net.minecraftforge.fml.common.Loader.sortModList(Loader.java:266)
	at net.minecraftforge.fml.common.Loader.loadMods(Loader.java:572)
	at net.minecraftforge.fml.server.FMLServerHandler.beginServerLoading(FMLServerHandler.java:98)
	at net.minecraftforge.fml.common.FMLCommonHandler.onServerStart(FMLCommonHandler.java:333)
	at net.minecraft.server.dedicated.DedicatedServer.func_71197_b(DedicatedServer.java:125)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:486)
	at java.lang.Thread.run(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at net.minecraftforge.fml.common.Loader.sortModList(Loader.java:266)
	at net.minecraftforge.fml.common.Loader.loadMods(Loader.java:572)

-- Sponge PhaseTracker --
Details:
	Phase Stack: [Empty stack]
Stacktrace:
	at net.minecraft.server.MinecraftServer.handler$onCrashReport$zje000(MinecraftServer.java:4721)
	at net.minecraft.server.MinecraftServer.func_71230_b(MinecraftServer.java:889)
	at net.minecraft.server.dedicated.DedicatedServer.func_71230_b(DedicatedServer.java:371)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:558)
	at java.lang.Thread.run(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_221, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 666668192 bytes (635 MB) / 1372061696 bytes (1308 MB) up to 1908932608 bytes (1820 MB)
	JVM Flags: 2 total; -Xms1G -Xmx2G
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2847 10 mods loaded, 10 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	Mods:

	| State | ID          | Version           | Source                                  | Signature |
	|:----- |:----------- |:----------------- |:--------------------------------------- |:--------- |
	| L     | minecraft   | 1.12.2            | minecraft.jar                           | None      |
	| L     | mcp         | 9.42              | minecraft.jar                           | None      |
	| L     | FML         | 8.0.99.99         | forge-1.12.2-14.23.5.2847-universal.jar | None      |
	| L     | forge       | 14.23.5.2847      | forge-1.12.2-14.23.5.2847-universal.jar | None      |
	| L     | spongeapi   | 7.1.0-0b58730f    | spongeforge-1.12.2-2838-7.1.7.jar       | None      |
	| L     | sponge      | 1.12.2-7.1.7      | spongeforge-1.12.2-2838-7.1.7.jar       | None      |
	| L     | spongeforge | 1.12.2-2838-7.1.7 | spongeforge-1.12.2-2838-7.1.7.jar       | None      |


	Plugins:

	| State | ID           | Version | Source                         | Signature |
	|:----- |:------------ |:------- |:------------------------------ |:--------- |
	| L     | pjc          | 0.3.0   | projectcore-7.2.0-0.3.0.jar    | None      |
	| L     | pjw          | 0.13.1  | projectworlds-7.2.0-0.13.1.jar | None      |
	| L     | virtualchest | 1.0.1   | VirtualChest-1.0.1.jar         | None      |

	Loaded coremods (and transformers): 
SpongeCoremod (spongeforge-1.12.2-2838-7.1.7.jar)
  org.spongepowered.common.launch.transformer.SpongeSuperclassTransformer
	Profiler Position: N/A (disabled)
	Is Modded: Definitely; Server brand changed to 'fml,forge,sponge'
	Type: Dedicated Server (map_server.txt)